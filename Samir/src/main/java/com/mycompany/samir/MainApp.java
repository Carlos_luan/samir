package com.mycompany.samir;

import java.io.IOException;
import java.util.GregorianCalendar;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class MainApp extends Application
{

    public WebDriver driver;
    public USer Usuario = new USer();
    private Stage primaryStage;

//abre a tela de usuário para login e triagem 
    @FXML
    void goTo(ActionEvent event)
    {

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        }
        catch (IOException ex)
        {

        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        //stage.setFullScreen(true);
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();

    }

    @FXML
    void goTo2(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/IndicePane.fxml"));
        }
        catch (IOException ex)
        {

        }

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.setTitle("Banco de Dados para Triagem");
        stage.show();
    }
// carrega a cena da interface root

    @Override
    public void start(Stage primaryStage) throws Exception
    {

        // Carrega o layout FXML
        Pane root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));

        // Cria a cena
        Scene scene = new Scene(root);

        // Define parâmetros para a janela
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(400);
        primaryStage.getIcons().add(new Image("/fxml/Chess51.png"));
        primaryStage.setTitle("Samir");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public static void main(String[] args) throws Exception
    {
        // Inicializa o JavaFX
        // Inicializa o JavaFX
        Banco banco = new Banco();
        banco.conectar();
        CalculoEInv calc = new CalculoEInv();
      calc.Exec_invertINPC("19/02/2019", 880, 10, "31/12/2019", "02/07/2019",true);
//        GregorianCalendar datadib = new GregorianCalendar();
        GeraRelatorio relatorio = new GeraRelatorio();
        relatorio.setRelatório(calc.RetornaTabela());
        relatorio.CriaDoc();
//        GregorianCalendar datacita = new GregorianCalendar();

       // calc.Exec_invertINPC("19/02/2019", 880, 10, "31/12/2019", "02/07/2019");
        //GregorianCalendar datadib = new GregorianCalendar();
        //GregorianCalendar datafinal = new GregorianCalendar();
        
        //datafinal.set(2018, 12, 31);
        //datadib.set(2019, 02, 19);
        //calc.correcao(datadib, datafinal);
        //  TaxaJuros taxinha = new TaxaJuros();
        //  System.out.println("Taxa:"+taxinha.TaxaAcumulada(datadib, datacita));
        launch(args);
    }
}
