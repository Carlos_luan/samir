/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

/**
 *
 * @author root
 */
public class Resultado
{

    private double rmi;
    private double IATM;
    private double taxaacumulada;
    private double corrigido;
    private double jurosApagar;
    private double soma;
    private double valorBeneficio;
    private String competencia;

    public double getRmi()
    {
        return rmi;
    }

    public void setRmi(double rmi)
    {
        this.rmi = rmi;
    }

    public double getIATM()
    {
        return IATM;
    }

    public void setIATM(double IATM)
    {
        this.IATM = IATM;
    }

    public double getTaxaacumulada()
    {
        return taxaacumulada;
    }

    public void setTaxaacumulada(double taxaacumulada)
    {
        this.taxaacumulada = taxaacumulada;
    }

    public double getCorrigido()
    {
        return corrigido;
    }

    public void setCorrigido(double corrigido)
    {
        this.corrigido = corrigido;
    }

    public double getJurosApagar()
    {
        return jurosApagar;
    }

    public void setJurosApagar(double jurosApagar)
    {
        this.jurosApagar = jurosApagar;
    }

    public double getSoma()
    {
        return soma;
    }

    public void setSoma(double soma)
    {
        this.soma = soma;
    }

    public double getValorBeneficio()
    {
        return valorBeneficio;
    }

    public void setValorBeneficio(double valorBeneficio)
    {
        this.valorBeneficio = valorBeneficio;
    }

    public String getCompetencia()
    {
        return competencia;
    }

    public void setCompetencia(String competencia)
    {
        this.competencia = competencia;
    }

}
