/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

/**
 *
 * @author carlo
 */
public class Dados {

    private String RMI="";
    private String Nprocesso="";
    private String Ajuizamento="";
    private String Dib="";
    private String FimCalc="";
    private String NParte="";
    private String Hono="";
    private String DataSent="";
    private String DataCita="";

    public String getDataCita() {
        return DataCita;
    }

    public void setDataCita(String DataCita) {
        this.DataCita = DataCita;
    }

    public String getRMI() {
        return RMI;
    }

    public void setRMI(String RMI) {
        this.RMI = RMI;
    }

    public String getNprocesso() {
        return Nprocesso;
    }

    public void setNprocesso(String Nprocesso) {
        this.Nprocesso = Nprocesso;
    }

    public String getAjuizamento() {
        return Ajuizamento;
    }

    public void setAjuizamento(String Ajuizamento) {
        this.Ajuizamento = Ajuizamento;
    }

    public String getDib() {
        return Dib;
    }

    public void setDib(String Dib) {
        this.Dib = Dib;
    }

    public String getFimCalc() {
        return FimCalc;
    }

    public void setFimCalc(String FimCalc) {
        this.FimCalc = FimCalc;
    }

    public String getNParte() {
        return NParte;
    }

    public void setNParte(String NParte) {
        this.NParte = NParte;
    }

    public String getHono() {
        return Hono;
    }

    public void setHono(String Hono) {
        this.Hono = Hono;
    }

    public String getDataSent() {
        return DataSent;
    }

    public void setDataSent(String DataSent) {
        this.DataSent = DataSent;
    }
}
