package com.mycompany.samir;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.applet.Applet;
import java.applet.AudioClip;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginController implements Initializable
{

    public WebDriver driver;
    public USer Usuario = new USer();
    private Stage primaryStage;
    public boolean btnTest = false;
    @FXML
    TableView<Dados> TableInf;
    @FXML
    TableColumn<Dados, String> NumProcesso;
    @FXML
    TableColumn<Dados, String> DibTable;
    @FXML
    TableColumn<Dados, String> NomParte;
    @FXML
    TableColumn<Dados, String> DataCit;
    @FXML
    TableColumn<Dados, String> DataFin;
//    @FXML
//    TableColumn<Dados, String> RmiTable;
    @FXML
    AnchorPane anchorPane;
    @FXML
    TextField etiquetinha;
    @FXML
    JFXTextField Login;
    @FXML
    JFXPasswordField Senha;
//botão de login

    @FXML
    public void handleEnterPressed(KeyEvent event) throws InterruptedException
    {
        if (event.getCode() == KeyCode.ENTER)
        {
            Login();
        }
    }

    @FXML
    private void Login() throws InterruptedException
    {

        String S;
        USer Usuario = new USer();
        Usuario.setLogin(Login.getText());
        Usuario.setSenha(Senha.getText());
        S = Login.getText();
        if (ValidaCpf.isCPF(S) == true)
        {
//            log.setText("-Login Iniciado!");
            System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
            // Usuário contem login e senha encapsulados.
            WebDriver driver = new FirefoxDriver();
            String url = "https://sapiens.agu.gov.br/login";
            driver.get(url);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            Thread.sleep(3000);

            Usuario.exec_login(driver, Usuario);
            this.driver = driver;
            btnTest = true;
        }
        else
        {
            JOptionPane.showMessageDialog(null, "CPF INVÁLIDO");
        }

    }

//botão executa triagem
    @FXML
    @SuppressWarnings("ObjectEqualsNull")
    private void handleNew(ActionEvent event) throws IOException
    {
        URL url = getClass().getResource("/SOUNDS/snes-startup.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        startTask();

    }

    @FXML
    public void alterar() throws IOException
    {
        Dados chave = new Dados();
        chave.setNParte(TableInf.getSelectionModel().getSelectedItem().getNParte());
        chave.setDataCita(TableInf.getSelectionModel().getSelectedItem().getDataCita().replaceAll("/", "-"));
        chave.setNprocesso(TableInf.getSelectionModel().getSelectedItem().getNprocesso());
        //  chave.setDib(TableInf.getSelectionModel().getSelectedItem().getDib());
        //    chave.setFimCalc(TableInf.getSelectionModel().getSelectedItem().getFimCalc());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditJf.fxml"));
        loader.setRoot(anchorPane);
        loader.setController(new EditController(chave));
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.setTitle("Editar Chaves");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.setScene(new Scene(root));
        stage.show();
    }
//botão retorna menu principal

    @FXML
    void RetornaMenu(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        }
        catch (IOException ex)
        {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        stage.setTitle("Mark-II");

    }

    public void runTask()
    {
        try
        {
            //instaciamento da classe  para buscar o processo a ser triado 
            buscaprocesso find = new buscaprocesso(driver);
            boolean teste = false;
            teste = find.find(driver);

            if (teste == false)
            {

                //sound by : https://freesound.org/people/Leszek_Szary/
                URL url = getClass().getResource("/SOUNDS/leszek-szary_success-2.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
//                spinner.setVisible(false);
//                log.setText("-Triagem finalizada");

            }
            else if (teste == true)
            {
                btnTest = false;
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
//                log.setText("-Erro de comunicação com plataforma Sapiens!");
//                spinner.setVisible(false);

            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            if (btnTest == true)
            {

                btnTest = false;
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
//                log.setText("-Erro de comunicação com plataforma Sapiens!");
//                spinner.setVisible(false);

            }
            else
            {
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Faça o Login primeiro");
//                log.setText("-Erro: Login Não Realizado!");

            }

        }

    }

    public void startTask()
    {
        // Create a Runnable
        Runnable task = new Runnable()
        {
            public void run()
            {
                runTask();
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();

    }

    @FXML
    public void Stop()
    {
        URL url = getClass().getResource("/SOUNDS/fupicat__videogame-death-sound.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        driver.quit();
    }

    @FXML
    public void atualizar()
    {
        try
        {
            List<Dados> dados = new ArrayList<>();
            Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            PreparedStatement stmt = connection.prepareStatement("select * from CAPA");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next())
            {
                Dados dadinho = new Dados();
                dadinho.setAjuizamento(resultSet.getString("AJUIZAMENTO"));
                dadinho.setDib(resultSet.getString("DIB"));
                dadinho.setNParte(resultSet.getString("PARTEAUTORA"));
                dadinho.setDataCita(resultSet.getString("DATA_CITA"));
                dadinho.setFimCalc(resultSet.getString("DATA_FINALCALC"));
                dadinho.setNprocesso(resultSet.getString("NPROCESSO"));
                dados.add(dadinho);
            }
            NumProcesso.setCellValueFactory(new PropertyValueFactory<Dados, String>("Nprocesso"));;
            NomParte.setCellValueFactory(new PropertyValueFactory<Dados, String>("Nparte"));
            DataCit.setCellValueFactory(new PropertyValueFactory<Dados, String>("DataCita"));
            DataFin.setCellValueFactory(new PropertyValueFactory<Dados, String>("FimCalc"));
            NomParte.setCellValueFactory(new PropertyValueFactory<Dados, String>("NParte"));
            DibTable.setCellValueFactory(new PropertyValueFactory<Dados, String>("Dib"));
            ObservableList<Dados> genericos = FXCollections.observableArrayList(dados);
            TableInf.setItems(genericos);

        }
        catch (SQLException ex)
        {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        atualizar();

    }
}