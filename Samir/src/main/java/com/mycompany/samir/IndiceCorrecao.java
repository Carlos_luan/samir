/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

/**
 *
 * @author carlo
 */
public class IndiceCorrecao
{
   private int Ano;
   private int mes;
   private double indcorrec;
   private String Tabela;

    public String getTabela()
    {
        return Tabela;
    }

    public void setTabela(String Tabela)
    {
        this.Tabela = Tabela;
    }
    public int getAno()
    {
        return Ano;
    }

    public void setAno(int Ano)
    {
        this.Ano = Ano;
    }

    public int getMes()
    {
        return mes;
    }

    public void setMes(int mes)
    {
        this.mes = mes;
    }

    public double getIndcorrec()
    {
        return indcorrec;
    }

    public void setIndcorrec(double indcorrec)
    {  
        this.indcorrec = indcorrec;
    }
}
