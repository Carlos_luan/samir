/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

/**
 *
 * @author carlo
 */
public class INDICE
{
    private int ano;
    private double mes1,mes2,mes3,mes4,mes5,mes6,mes7,mes8,mes9,mes10,mes11,mes12;

    public int getAno()
    {
        return ano;
    }

    public void setAno(int ano)
    {
        this.ano = ano;
    }

    public double getMes1()
    {
        return mes1;
    }

    public void setMes1(double mes1)
    {
        this.mes1 = mes1;
    }

    public double getMes2()
    {
        return mes2;
    }

    public void setMes2(double mes2)
    {
        this.mes2 = mes2;
    }

    public double getMes3()
    {
        return mes3;
    }

    public void setMes3(double mes3)
    {
        this.mes3 = mes3;
    }

    public double getMes4()
    {
        return mes4;
    }

    public void setMes4(double mes4)
    {
        this.mes4 = mes4;
    }

    public double getMes5()
    {
        return mes5;
    }

    public void setMes5(double mes5)
    {
        this.mes5 = mes5;
    }

    public double getMes6()
    {
        return mes6;
    }

    public void setMes6(double mes6)
    {
        this.mes6 = mes6;
    }

    public double getMes7()
    {
        return mes7;
    }

    public void setMes7(double mes7)
    {
        this.mes7 = mes7;
    }

    public double getMes8()
    {
        return mes8;
    }

    public void setMes8(double mes8)
    {
        this.mes8 = mes8;
    }

    public double getMes9()
    {
        return mes9;
    }

    public void setMes9(double mes9)
    {
        this.mes9 = mes9;
    }

    public double getMes10()
    {
        return mes10;
    }

    public void setMes10(double mes10)
    {
        this.mes10 = mes10;
    }

    public double getMes11()
    {
        return mes11;
    }

    public void setMes11(double mes11)
    {
        this.mes11 = mes11;
    }

    public double getMes12()
    {
        return mes12;
    }

    public void setMes12(double mes12)
    {
        this.mes12 = mes12;
    }
}
