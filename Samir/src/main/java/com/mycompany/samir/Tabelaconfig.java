/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class Tabelaconfig
{

    public List<INDICE> inpc()
    {
        List<INDICE> indice = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            PreparedStatement stmt = connection.prepareStatement("select * from INPC order by ANO DESC ");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next())
            {
                INDICE key = new INDICE();
                key.setAno(resultSet.getInt(1));
                key.setMes1(resultSet.getDouble(2));
                key.setMes2(resultSet.getDouble(3));
                key.setMes3(resultSet.getDouble(4));
                key.setMes4(resultSet.getDouble(5));
                key.setMes5(resultSet.getDouble(6));
                key.setMes6(resultSet.getDouble(7));
                key.setMes7(resultSet.getDouble(8));
                key.setMes8(resultSet.getDouble(9));
                key.setMes9(resultSet.getDouble(10));
                key.setMes10(resultSet.getDouble(11));
                key.setMes11(resultSet.getDouble(12));
                key.setMes12(resultSet.getDouble(13));
                indice.add(key);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Tabelaconfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return indice;
    }

    public List<INDICE> ipcae()
    {
        List<INDICE> indice = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            PreparedStatement stmt = connection.prepareStatement("select * from IPCA_E ORDER BY ANO DESC");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next())
            {
                INDICE index = new INDICE();
                index.setAno(resultSet.getInt(1));
                index.setMes1(resultSet.getDouble(2));
                index.setMes2(resultSet.getDouble(3));
                index.setMes3(resultSet.getDouble(4));
                index.setMes4(resultSet.getDouble(5));
                index.setMes5(resultSet.getDouble(6));
                index.setMes6(resultSet.getDouble(7));
                index.setMes7(resultSet.getDouble(8));
                index.setMes8(resultSet.getDouble(9));
                index.setMes9(resultSet.getDouble(10));
                index.setMes10(resultSet.getDouble(11));
                index.setMes11(resultSet.getDouble(12));
                index.setMes12(resultSet.getDouble(13));
                indice.add(index);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Tabelaconfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return indice;
    }

    public List<INDICE> salmin() {
        List<INDICE> prov = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            PreparedStatement stmt = connection.prepareStatement("select * from SALARIOMIN");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
               INDICE key = new INDICE();
                key.setAno(resultSet.getInt(1));
                key.setMes1(resultSet.getDouble(2));
                prov.add(key);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tabelaconfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }
}
