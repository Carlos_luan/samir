/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class IndiceController implements Initializable
{

    Tabelaconfig refresh = new Tabelaconfig();
    @FXML
    TableView<INDICE> InpcTable;
    @FXML
    TableColumn InpcAno, Inpc1, Inpc2, Inpc3, Inpc4, Inpc5, Inpc6, Inpc7, Inpc8, Inpc9, Inpc10, Inpc11, Inpc12;
    @FXML
    TableView<INDICE> IpcaTable;
    @FXML
    TableColumn IpcAno, Ipca1, Ipca2, Ipca3, Ipca4, Ipca5, Ipca6, Ipca7, Ipca8, Ipca9, Ipca10, Ipca11, Ipca12;
    @FXML
    TableView<INDICE> TableSal;
    @FXML
    TableColumn ColAno, ColValor;
    @FXML
    JFXDatePicker inpcInsert, UpdateInpc, UpdateIpca;
    @FXML
    JFXTextField UptIndiceInpc, UptIndiceIpca;

    @FXML
    void InserirIpca()
    {

    }

    @FXML
    void excluirIpca()
    {

    }

    @FXML
    void InserirInpc()
    {

    }

    @FXML
    void excluirInpc()
    {

    }

    @FXML
    void AlterarIpca()
    {
        Banco banco = new Banco();
        IndiceCorrecao indice = new IndiceCorrecao();
        try
        {
            indice.setAno(UpdateIpca.getValue().getYear());
            indice.setMes(UpdateIpca.getValue().getMonth().getValue());
        }
        catch (Exception err)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro");
            alert.initStyle(StageStyle.DECORATED);
            alert.setHeaderText("Erro 333*2");
            alert.setContentText("Preencha o Campo de Data do Índice à ser Alterado");
            alert.showAndWait();
        }

        try
        {
            indice.setIndcorrec(Integer.parseInt(UptIndiceIpca.getText()));
        }
        catch (NumberFormatException err)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro");
            alert.initStyle(StageStyle.DECORATED);
            alert.setHeaderText("Erro 333*2");
            alert.setContentText("Não insira Valores textuais em Campo numérico");
            alert.showAndWait();
        }
        atualizarTabel();
    }

    @FXML
    void AlterarInpc()
    {
        Banco banco = new Banco();
        IndiceCorrecao indice = new IndiceCorrecao();
        indice.setTabela("INPC");
        try
        {
            indice.setAno(UpdateInpc.getValue().getYear());
            indice.setMes(UpdateInpc.getValue().getMonth().getValue());
        }
        catch (Exception err)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro");
            alert.initStyle(StageStyle.DECORATED);
            alert.setHeaderText("Erro 333*2");
            alert.setContentText("Preencha o Campo de Data do Índice à ser Alterado");
            alert.showAndWait();
        }

        try
        {
            indice.setIndcorrec(Double.parseDouble(UptIndiceInpc.getText()));
        }
        catch (NumberFormatException err)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro");
            alert.initStyle(StageStyle.DECORATED);
            alert.setHeaderText("Erro 333*2");
            alert.setContentText("Não insira Valores textuais em Campo numérico");
            alert.showAndWait();
        }
        banco.atualizarIndice(indice);
        atualizarTabel();
    }

    void atualizarTabel()
    {

        /*
        inicialização da tabela INPC
         */
        InpcAno.setCellValueFactory(new PropertyValueFactory<INDICE, String>("ano"));
        Inpc1.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes1"));
        Inpc2.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes2"));
        Inpc3.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes3"));
        Inpc4.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes4"));
        Inpc5.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes5"));
        Inpc6.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes6"));
        Inpc7.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes7"));
        Inpc8.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes8"));
        Inpc9.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes9"));
        Inpc10.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes10"));
        Inpc11.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes11"));
        Inpc12.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes12"));
        ObservableList<INDICE> genericos = FXCollections.observableArrayList(refresh.inpc());
        InpcTable.setItems(genericos);
        /*
        inicialização da tabela IPCA-E
         */
        IpcAno.setCellValueFactory(new PropertyValueFactory<INDICE, String>("ano"));
        Ipca1.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes1"));
        Ipca2.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes2"));
        Ipca3.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes3"));
        Ipca4.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes4"));
        Ipca5.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes5"));
        Ipca6.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes6"));
        Ipca7.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes7"));
        Ipca8.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes8"));
        Ipca9.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes9"));
        Ipca10.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes10"));
        Ipca11.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes11"));
        Ipca12.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes12"));
        ObservableList<INDICE> ipcae = FXCollections.observableArrayList(refresh.ipcae());
        IpcaTable.setItems(ipcae);
        /*
        inicialização da tabela SALARIO MINIMO
         */
        ColAno.setCellValueFactory(new PropertyValueFactory<INDICE, String>("ano"));
        ColValor.setCellValueFactory(new PropertyValueFactory<INDICE, String>("mes1"));
        ObservableList<INDICE> SAL = FXCollections.observableArrayList(refresh.salmin());
        TableSal.setItems(SAL);
    }

    @FXML
    void RetornaMenu(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        }
        catch (IOException ex)
        {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        stage.setTitle("Samir 0.1");

    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        atualizarTabel();
    }

}
