/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root2
 */
public class GetDados
{

    public List GetDados(String capa)
    {
        List<String> dados = new ArrayList();
        dados.add(capa.substring(capa.indexOf("Ajuizamento:"), capa.indexOf("Ajuizamento:") + 24).replace("Ajuizamento:", "").trim());//CAPTURA AJUIZAMENTO
//        dados.add(capa.substring(capa.indexOf("Valor da Causa:"), capa.indexOf("Eletrônico:")).replace("Valor da Causa:", "").trim());// || VALOR DA CAUSA
        String parte = capa;
        parte = parte.substring(parte.lastIndexOf("Representado AGU") + 17, parte.lastIndexOf("REQUERENTE (PÓLO ATIVO)")); // necessário para capturar nome da parte
        dados.add(parte.substring(0, parte.indexOf(" (")));//nome da parte
        dados.add(capa.substring(capa.indexOf("Número Único (CNJ):") + 18, capa.indexOf("Classe:")).replace(":", "").trim());
        dados.add(capa.substring(capa.indexOf("Abertura:") + 9, capa.indexOf("Volume(s):") - 7).trim());
        return dados; 
       
    }
}