package com.mycompany.samir;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * NESSA CLASSE SERÁ REALIZADO O CÁLCULO DE EXECUÇÃO INVERTIDA
 */
public class CalculoEInv
{

    public List correcao(GregorianCalendar datadib, GregorianCalendar datafinal) throws SQLException
    {
        int mesComeca;
        int mesAcaba;
        Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
        List tabela = new ArrayList();
        PreparedStatement inicio = connection.prepareStatement("SELECT *FROM INPC where ANO = " + datadib.get(datadib.YEAR) + ";");
        ResultSet result = inicio.executeQuery();
        double indice = (result.getInt(datadib.get(datadib.MONTH)) / 100) + 1;
        tabela.add(indice);
        for (int i = datadib.get(datadib.YEAR); i <= datafinal.get(datafinal.YEAR); i++)
        {
            if (i != datadib.get(datadib.YEAR))
            {
                mesComeca = 1;
            }
            else
            {
                mesComeca = datadib.get(datadib.MONTH);
            }
            if (i == datafinal.get(datafinal.YEAR))
            {
                mesAcaba = datafinal.get(datafinal.MONTH);
            }
            else
            {
                mesAcaba = 12;
            }
            for (int j = mesComeca; j <= mesAcaba; j++)
            {
                if (result.getDouble(j + 1) <= 0)
                {
                    indice = indice * 1;
                }
                else
                {
                    indice = indice * (result.getDouble(j + 1) / 100) + 1;
//                    System.out.println("Indice: " + indice);
//                    System.out.println("Competencia: " + j + "/" + i);
                    tabela.add(indice);
                }
            }
        }

        return tabela;
    }

    public Resultado calculo(String competencia, double rmi, double IATM)
    {
        Resultado resultinho = new Resultado();
        double corrigido = ((rmi * (IATM)));
        double JurosAPagar = corrigido * (taxaacumulada);
        double soma = corrigido + JurosAPagar;
        resultinho.setCompetencia(competencia);
        resultinho.setRmi(rmi);
        resultinho.setIATM(IATM);
        resultinho.setTaxaacumulada(taxaacumulada);
        resultinho.setCorrigido(corrigido);
        resultinho.setJurosApagar(JurosAPagar);
        resultinho.setSoma(soma);
        return resultinho;
    }
    double taxaacumulada = 0;
    String printardoc = "\n";

    public void Exec_invertINPC(String dib, double rmi, double Juros, String DataFinal, String DataCita, boolean sal) throws SQLException
    {
        double IATM;
        boolean salarioMin = false;
        String[] dataSplitDIB = dib.split("/");
        String[] dataSplitFinal = DataFinal.split("/");
        String[] dataCitaFinal = DataCita.split("/");
        GregorianCalendar datadib = new GregorianCalendar();
        GregorianCalendar datafinal = new GregorianCalendar();
        GregorianCalendar datacita = new GregorianCalendar();
        datacita.set(Integer.parseInt(dataCitaFinal[2]), Integer.parseInt(dataCitaFinal[1]), Integer.parseInt(dataCitaFinal[0]));
        datafinal.set(Integer.parseInt(dataSplitFinal[2]), Integer.parseInt(dataSplitFinal[1]), Integer.parseInt(dataSplitFinal[0]));
        datadib.set(Integer.parseInt(dataSplitDIB[2]), Integer.parseInt(dataSplitDIB[1]), Integer.parseInt(dataSplitDIB[0]));
        System.out.println("dia:" + datadib.get(datadib.DATE) + "mes:" + datadib.get(datadib.MONTH) + "ano:" + datadib.get(datadib.YEAR));
        int mesComeca = 0;
        int mesAcaba = 0;
        TaxaJuros taxinha = new TaxaJuros();
        taxaacumulada = taxinha.TaxaAcumulada(datadib, datacita);
        Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
        PreparedStatement SRM;
        ResultSet ResultSet;
        DecimalFormat df = new DecimalFormat("#,##00%");
        List indice = correcao(datadib, datafinal);
        int ind = indice.size() - 1;
        double valorbeneficio = 0;
        ResultSet resultsal;
        List<Resultado> resultado = new ArrayList<>();
        for (int i = datadib.get(datadib.YEAR); i <= datafinal.get(datafinal.YEAR); i++)
        {
            if (i != datadib.get(datadib.YEAR))
            {
                mesComeca = 1;
            }
            else
            {
                mesComeca = datadib.get(datadib.MONTH);
            }
            if (i == datafinal.get(datafinal.YEAR))
            {
                mesAcaba = datafinal.get(datafinal.MONTH);
            }
            else
            {
                mesAcaba = 12;
            }
            for (int j = mesComeca; j <= mesAcaba; j++)
            {

                SRM = connection.prepareStatement("SELECT ANO,\n"
                        + "       VALOR\n"
                        + "  FROM SALARIOMIN WHERE ANO = " + (i) + ";");
                resultsal = SRM.executeQuery();
                rmi = resultsal.getDouble("VALOR");

                if (i == datadib.get(datadib.YEAR) && j == datadib.get(datadib.MONTH))
                {
                    int dia = datadib.get(datacita.DAY_OF_MONTH);
                    int totaldia = datadib.getActualMaximum(datadib.DAY_OF_MONTH);
                    double porcent = ((dia * 100) / totaldia);
                    rmi = (rmi - (rmi * (porcent / 100)));
                }

                if (datacita.get(datacita.YEAR) <= i)
                {

                    if (((datacita.get(datacita.YEAR) == i) && (datacita.get(datacita.MONTH) > j)))
                    {

                    }
                    else
                    {

                        taxaacumulada = taxaacumulada - 0.005;
                        if (taxaacumulada < 0)
                        {
                            taxaacumulada = 0;
                        }
                    }
                }
                IATM = (double) indice.get(ind);
                if (j == 12 && sal)
                {
                    Resultado resultinho = new Resultado();
                    double rmitemp;
                    double porcent = (((mesAcaba - mesComeca) * 100) / 12);
                    rmitemp = ((rmi*porcent)/100);
                    resultinho = calculo(j + 1 + "/" + i, rmitemp, IATM);
                    valorbeneficio = valorbeneficio + resultinho.getSoma();
                    resultinho.setValorBeneficio(valorbeneficio);
                    printardoc = "---------------------------" + printardoc + ("competencia: " + resultinho.getCompetencia() + "\n" + "RMI: " + rmitemp + "\n" + "IATM: " + IATM + "\n" + "Juros:"
                            + taxaacumulada + "\n" + "corrigido: " + resultinho.getCorrigido() + "\n" + "JurosAPagar: " + resultinho.getJurosApagar() + "\n" + "SOMA: "
                            + String.format("$%,.2f", resultinho.getSoma()) + "\n" + "---------------------------" + "\n");
                }
                Resultado resultinho = new Resultado();
                // double corrigido = (rmi + (rmi * (IATM)));
                resultinho = calculo(j + "/" + i, rmi, IATM);
                valorbeneficio = valorbeneficio + resultinho.getSoma();
                resultinho.setValorBeneficio(valorbeneficio);
                ind--;
                resultado.add(resultinho);
                printardoc = "---------------------------" + printardoc + ("competencia: " + resultinho.getCompetencia() + "\n" + "RMI: " + rmi + "\n" + "IATM: " + IATM + "\n" + "Juros:"
                        + taxaacumulada + "\n" + "corrigido: " + resultinho.getCorrigido() + "\n" + "JurosAPagar: " + resultinho.getJurosApagar() + "\n" + "SOMA: "
                        + String.format("$%,.2f", resultinho.getSoma()) + "\n" + "---------------------------" + "\n");
            }
        }
        // System.out.println("\n\n\n VALOR DO BENEFICIO: " + String.format("$%,.2f", valorbeneficio));
        System.out.println("\n" + "\n" + printardoc + "\n\n" + String.format("$%,.2f", valorbeneficio));
        printardoc = printardoc + "\n\n" + String.format("$%,.2f", valorbeneficio);
    }

    public String RetornaTabela()
    {
        return printardoc;
    }
}
