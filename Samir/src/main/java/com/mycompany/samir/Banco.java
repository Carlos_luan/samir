/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Banco
{

    public void conectar()
    {
        try
        {
            // inicia a conecção com o banco de dados
            Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            System.out.println("Conexão realizada !!!!");

            Statement comandoSql = connection.createStatement();
            // criando uma tabela
            comandoSql.execute("CREATE TABLE IF NOT EXISTS CAPA (\n"
                    + "    NPROCESSO      VARCHAR (100) NOT NULL,\n"
                    + "    AJUIZAMENTO    VARCHAR (10),\n"
                    + "    RMI            DOUBLE,\n"
                    + "    DIB            VARCHAR,\n"
                    + "    PARTEAUTORA    VARCHAR (100),\n"
                    + "    DATA_CITA      VARCHAR (10),\n"
                    + "    DATA_FINALCALC VARCHAR (10),\n"
                    + "    PRIMARY KEY (\n"
                    + "        NPROCESSO\n"
                    + "    )\n"
                    + ");");

            comandoSql.execute("CREATE TABLE IF NOT EXISTS CORRMONEY (\n"
                    + "    ANO      INT    NOT NULL,\n"
                    + "    CORRECAO DOUBLE NOT NULL\n"
                    + "                    DEFAULT 1,\n"
                    + "    PRIMARY KEY (\n"
                    + "        ANO\n"
                    + "    )\n"
                    + ");");
            comandoSql.execute("CREATE TABLE IF NOT EXISTS PLANCALCULO (\n"
                    + "    NPROCESSO   VARCHAR (100) REFERENCES CAPA (NPROCESSO) \n"
                    + "                              NOT NULL,\n"
                    + "    COMPETENCIA VARCHAR (10)  NOT NULL,\n"
                    + "    DEVIDO      DOUBLE        NOT NULL,\n"
                    + "    IND_ATM     DOUBLE        NOT NULL,\n"
                    + "    CORRIGIDO   DOUBLE        NOT NULL,\n"
                    + "    [JUROS%]    DOUBLE        NOT NULL,\n"
                    + "    JUROS       DOUBLE        NOT NULL,\n"
                    + "    SOMA        DOUBLE,\n"
                    + "    ID          INTEGER       PRIMARY KEY AUTOINCREMENT\n"
                    + ");");
            comandoSql.execute("CREATE TABLE IF NOT EXISTS INPC (\n"
                    + "    ANO  INTEGER NOT NULL\n"
                    + "                 PRIMARY KEY,\n"
                    + "    [1]  DOUBLE  DEFAULT (1),\n"
                    + "    [2]  DOUBLE  DEFAULT (1),\n"
                    + "    [3]  DOUBLE  DEFAULT (1),\n"
                    + "    [4]  DOUBLE  DEFAULT (1),\n"
                    + "    [5]  DOUBLE  DEFAULT (1),\n"
                    + "    [6]  DOUBLE  DEFAULT (1),\n"
                    + "    [7]  DOUBLE  DEFAULT (1),\n"
                    + "    [8]  DOUBLE  DEFAULT (1),\n"
                    + "    [9]  DOUBLE  DEFAULT (1),\n"
                    + "    [10] DOUBLE  DEFAULT (1),\n"
                    + "    [11] DOUBLE  DEFAULT (1),\n"
                    + "    [12] DOUBLE  DEFAULT (1) \n"
                    + ");");
            comandoSql.execute("CREATE TABLE IF NOT EXISTS IPCA_E (\n"
                    + "    ANO   INTEGER NOT NULL\n"
                    + "                  PRIMARY KEY,\n"
                    + "    [1]   DOUBLE,\n"
                    + "    [2]   DOUBLE,\n"
                    + "    [3]   DOUBLE,\n"
                    + "    ACU_1 DOUBLE,\n"
                    + "    [4]   DOUBLE,\n"
                    + "    [5]   DOUBLE,\n"
                    + "    [6]   DOUBLE,\n"
                    + "    ACU_2 DOUBLE,\n"
                    + "    [7]   DOUBLE,\n"
                    + "    [8]   DOUBLE,\n"
                    + "    [9]   DOUBLE,\n"
                    + "    ACU_3 DOUBLE,\n"
                    + "    [10]  DOUBLE,\n"
                    + "    [11]  DOUBLE,\n"
                    + "    [12]  DOUBLE\n"
                    + ");");
            comandoSql.execute("CREATE TABLE IF NOT EXISTS SALARIOMIN (\n"
                    + "    ANO   INTEGER NOT NULL\n"
                    + "                  PRIMARY KEY,\n"
                    + "    VALOR DOUBLE\n"
                    + ");");
            comandoSql.execute("CREATE TABLE IF NOT EXISTS poupanca (\n"
                    + "    mes_ano VARCHAR (8) PRIMARY KEY,\n"
                    + "    TAXA DOUBLE\n"
                    + ");");
            //desconectando do banco de dados
            connection.close();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void InserirAutomatizado(List dados)
    {
        /*
         * Lista de entradas para o get
         * 0= ajuizamento
         * 1= Nome da parte
         * 2= Numero do processo
         * 3= Data da Abertura (citação)
         */
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            Statement InserirCapinha = connection.createStatement();
            InserirCapinha.execute("INSERT INTO CAPA (\n"
                    + "                     NPROCESSO,\n"
                    + "                     AJUIZAMENTO,\n"
                    + "                     DIB,\n"
                    + "                     PARTEAUTORA,\n"
                    + "                     DATA_CITA,\n"
                    + "                     DATA_FINALCALC\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '" + dados.get(2) + "',\n"
                    + "                     '" + dados.get(0) + "',\n"
                    + "                     'NULL',\n"
                    + "                     '" + dados.get(1) + "',\n"
                    + "                     '" + dados.get(3) + "',\n"
                    + "                     'NULL'\n"
                    + "                 );");
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inserirIndice(IndiceCorrecao dados)
    {
        try
        {
            Connection conection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            Statement atualizar = conection.createStatement();
            atualizar.execute("INSERT INTO "+dados.getTabela()+" (ANO,[1]) VALUES ("+dados.getAno()+","+dados.getIndcorrec()
                    +");");
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizarIndice(IndiceCorrecao dados)
    {
        try
        {
            Connection conection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            Statement atualizar = conection.createStatement();
            atualizar.execute("UPDATE " + dados.getTabela() + "\n"
                    + "   SET [" + dados.getMes() + "] = " + dados.getIndcorrec() + "\n"
                    + " WHERE ANO = " + dados.getAno() + "");
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void AtualizarCapa(String Ajuizamento, String Dib, String ParteAutora, String DCita, String DFim, String Nprocesso)
    {
        try
        {
            Connection conection = DriverManager.getConnection("jdbc:sqlite:SAMIRBANK.db");
            Statement atualizar = conection.createStatement();
            atualizar.execute("UPDATE CAPA\n"
                    + "   SET NPROCESSO = '" + Nprocesso + "',"
                    + "AJUIZAMENTO = '" + Ajuizamento + "',\n"
                    + "       DIB = '" + Dib + "',\n"
                    + "       PARTEAUTORA = '" + ParteAutora + "',\n"
                    + "       DATA_CITA = '" + DCita + "',\n"
                    + "       DATA_FINALCALC = '" + DFim + "'\n"
                    + " WHERE NPROCESSO = '" + Nprocesso + "';");
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
