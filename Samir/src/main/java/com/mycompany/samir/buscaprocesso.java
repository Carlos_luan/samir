/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class buscaprocesso
{

    public buscaprocesso(WebDriver driver)
    {
        super();
        // TODO Auto-generated constructor stub
    }

    @SuppressWarnings("SleepWhileInLoop")
    public boolean find(WebDriver driver) throws InterruptedException, UnsupportedFlavorException, IOException, SQLException
    {
        Etiqueta etiqueta = new Etiqueta();
        GetDados getGados = new GetDados();
        /*
         * Foi necessário criar um delay entre a execução e o tempo de espera
         * para carregar a pagina web
         */
        Thread.sleep(5000);
        Actions action = new Actions(driver);

        //transforma a tabela num webelement manipulavel
        Thread.sleep(6500);
        //localiza a tabela onde estão armazenadas as tarefas
        // WebDriverWait wait = new WebDriverWait (driver,30);
        // wait.until(ExpectedConditions.presenceOfElementLocated(By.id("gridview-1104-body")));
        //caso o robo não clique no processo, mude o endereço disso aqui ass: luanzinho e eduzinho
        WebElement tabela = driver.findElement(By.id("gridview-1109-table"));
        //cria uma lista das tarefas na tela do usuário
        List<WebElement> tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
        /*
         * recebe o tamanho da lista, necessário para se realizar o laço e
         * executar a triagem sem a necessidade de intervenção do cliente
         */
        int tamanho = tarefas.size();
        // laço que executa a realização da triagem
        while (tamanho > 0)
        {
            // verifica a existencia da tabela e retorna lista vazia caso não haja tarefas
            try
            {
                //caso o robo não clique no processo, mude o endereço disso aqui ass: luanzinho e eduzinho
                tabela = driver.findElement(By.id("gridview-1109-table"));
                tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
                Thread.sleep(10000);
                System.out.println(tamanho);
                //clica no primeiro elemento da tabela
                driver.findElement(By.xpath("//tr[1]/td[3]/div/a")).click();
                // driver.findElement(By.xpath("//tr[" + tarefasT.size() + "]/td[2]/div/span/span[2]")).click();   
                // loading do carregamento do arquivo
                Thread.sleep(3000);
                //Clica do lado direito da tela
                Thread.sleep(7000);
                //  cria uma lista com o endereçi de memoria das janelas abertas no navegador
                List<String> janela = new ArrayList(driver.getWindowHandles());
                //seleciona a janela da tarefa aberta
                driver.switchTo().window(janela.get(1));

                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                Tela2.click();

                //Simula Ctrl+A (SELECIONA TODOS) / Ctrl+C (COPIAR)
                action = new Actions(driver);

                action.sendKeys(Keys.END).build().perform();
                action.sendKeys(Keys.PAGE_UP).build().perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                Thread.sleep(1000);
                // colando documento do clipboard para uma string (CTRL+V) (COLAR)

            }
            catch (Exception e)
            {
                tarefas.clear();
                return false;
            }
            try
            {
                Thread.sleep(7000);
                //  cria uma lista com o endereçi de memoria das janelas abertas no navegador
                List<String> janela = new ArrayList(driver.getWindowHandles());
                //seleciona a janela da tarefa aberta
                driver.switchTo().window(janela.get(1));

                // compara palavra chave
                // fecha a segunda janela e volta para primeira janela
                driver.switchTo().window(janela.get(1)).close();
                driver.switchTo().window(janela.get(0));
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                DataFlavor flavor = DataFlavor.stringFlavor;
                String Dados = clipboard.getData(flavor).toString();
                List<String> dados = new ArrayList();
                dados = getGados.GetDados(Dados);
                /*
                 * Lista de entradas para o get
                 * 0= ajuizamento
                 * 1= Nome da parte
                 * 2= Numero do processo
                 * 3= Data da Abertura (citação)
                 */
                Banco banco = new Banco();
                banco.InserirAutomatizado(dados);
                Thread.sleep(4000);
                etiqueta.exec_etiqueta(driver);
                
                

            }
            catch (Exception e)
            {
                return true;

            }
        }
        return false;
    }
}
