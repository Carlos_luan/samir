/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Edu
 */
public class GeraRelatorio  {
    public int paragrafoNum = 0;
    public String relatório = "";
    public String NomeDoc = "padrao";
    public GeraRelatorio() {
    }

    public void setParagrafoNum(int paragrafoNum) {
        this.paragrafoNum = paragrafoNum;
    }

    public void setRelatório(String relatório) {
        this.relatório = relatório;
    }
public void CriaDoc() throws FileNotFoundException{
      // criação do documento
          Document document = new Document();
          try {
              
              PdfWriter.getInstance(document, new FileOutputStream("D:\\"+NomeDoc+".pdf"));
              document.open();
              int i;
             for(i=0;i<=paragrafoNum;i++){ 
              // adicionando um parágrafo no documento
              document.add(new Paragraph("Gerando PDF - Java"+"\n"+relatório));
             }
    }
          catch(DocumentException de) {
              System.err.println(de.getMessage());
          }
          catch(IOException ioe) {
              System.err.println(ioe.getMessage());
          }
          document.close();
      }   

    public void setNomeDoc(String NomeDoc) {
        this.NomeDoc = NomeDoc;
    }

}
