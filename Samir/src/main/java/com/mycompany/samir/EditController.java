/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.samir;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author root2
 */
public class EditController implements Initializable
{

    private Dados chave;

    public EditController(Dados chave)
    {
        this.chave = chave;
    }
    @FXML
    JFXTextField ProcessoTxt, NParte;
    @FXML
    JFXDatePicker dib, ajuizamento, citacao, dip;

    @FXML
    public void Alterar(ActionEvent event)
    {
        String Dib = null;
        String Ajuizamento = null;
        String Citacao = null;
        String Dip = null;
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        Banco banco = new Banco();
        String processo = ProcessoTxt.getText().toUpperCase().replace("'", "").replace("´", "").trim();
        String nparte = NParte.getText().toUpperCase().replace("'", "").replace("´", "").trim();
        try
        {
            Dib = dib.getValue().format(dateFormatter).toString().trim();
            Ajuizamento = ajuizamento.getValue().format(dateFormatter).toString().trim();
            Citacao = citacao.getValue().format(dateFormatter).toString().trim();
            Dip = dip.getValue().format(dateFormatter).toString().trim();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira valores em branco");
        }

        if ((processo.equals(null)) || nparte.equals(null))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");

        }
        else
        {

            banco.AtualizarCapa(Ajuizamento, Dib, nparte, Citacao, Dip, processo);
            JOptionPane.showMessageDialog(null, "Dados Atualizados");
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = null;
            stage.close();
        }

    }

    @FXML
    public void cancelar(ActionEvent event)
    {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        stage.close();
    }

    public LocalDate localDate(String dib)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = LocalDate.parse(dib.trim(), formatter);
        return localDate;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        ProcessoTxt.setText(chave.getNprocesso());
        NParte.setText(chave.getNParte());
        if (!chave.getDib().isEmpty())
        {
            dib.setValue(localDate(chave.getDib()));
        }
        if (!chave.getDataCita().isEmpty())
        {
            citacao.setValue(localDate(chave.getDataCita()));
        }
        if (!chave.getFimCalc().isEmpty())
        {
            dip.setValue(localDate(chave.getFimCalc()));
        }
    }

}
